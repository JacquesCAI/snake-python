import pygame
import pygame_menu

from game import Game

pygame.init()
settings = {
    'bad_apple' : False,
    'super_apple' : False,
    'ghost_apple' : False,
    'toric_grid' : False,
    'random_walls' : False,
}

scr = pygame.display.set_mode((1000, 800))
pygame.display.set_caption('SnakeTron')

def start_the_game():
    game = Game(scr, settings)
    game.start()

def set_bad_apple(*value):
    settings['bad_apple'] = value[1]

def set_super_apple(*value):
    settings['super_apple'] = value[1]

def set_ghost_apple(*value):
    settings['ghost_apple'] = value[1]

def set_random_walls(*value):
    settings['random_walls'] = value[1]

menu = pygame_menu.Menu('Welcome', 800, 800, theme=pygame_menu.themes.THEME_BLUE)

menu.add.button('Play', start_the_game)
menu.add.selector('Bad apple :', [('False', False), ('True', True)], onchange=set_bad_apple)
menu.add.selector('Super apple :', [('False', False), ('True', True)], onchange=set_super_apple)
menu.add.selector('Ghost apple :', [('False', False), ('True', True)], onchange=set_ghost_apple)
menu.add.selector('Random walls :', [('False', False), ('True', True)], onchange=set_random_walls)
menu.add.button('Quit', pygame_menu.events.EXIT)

menu.mainloop(scr)