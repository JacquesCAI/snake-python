import pygame
import random
    
class Snake:
    def __init__(self, scr, color):
        self.scr = scr
        self.go_to = ""
        self.x = random.randint(0, 1000) // 20 * 20
        self.y = random.randint(0, 800) // 20 * 20
        self.color = color
        self.size = 20
        self.height = 1
        self.coos = []
        self.score = 0
        self.ghost = False

    def changeDirection(self):
        if self.go_to == "LEFT":
            self.x += -self.size
        elif self.go_to == "RIGHT":
            self.x += self.size
        elif self.go_to == "UP":
            self.y += -self.size
        elif self.go_to == "DOWN":
            self.y += self.size