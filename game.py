import pygame
import time
import random

from snake import Snake
from food import Food

class Game:
    def __init__(self, scr, settings):
        self.scr = scr
        self.game_over = False
        self.snakes = [Snake(scr, (0,0,255)), Snake(scr, (0,191,255))]
        self.food = Food(scr)
        self.color = (144,238,144)
        self.best_score = self.getBestScore()
        self.settings = settings
        self.walls = []

    def end(self):
        self.saveScore()
        
        display = pygame.font.SysFont(None, 25)
        value = display.render("Fin", True, (255, 0, 0))
        self.scr.blit(value, [1000/2, 800/2])

        for k, snake in enumerate(self.snakes):
            display = pygame.font.SysFont(None, 25)
            
            if k == 0:
                value = display.render("Score: " + str(snake.score), True, snake.color)
                self.scr.blit(value, [1000/2, 800/2 + 30])
            else:
                value = display.render("Score: " + str(snake.score), True, snake.color)
                self.scr.blit(value, [1000/2, 800/2 + 60])

        pygame.display.update()
        time.sleep(5)

    def setRandomWalls(self):
        for wall in range(random.randint(4, 6)):
            direction = random.choice(['hor', 'ver'])
            x = random.randint(0, 1000) // 20 * 20
            y = random.randint(0, 800) // 20 * 20
            tmp_wall = []
            for square in range(random.randint(3, 6)):
                tmp_wall.append({'x': x, 'y': y})
                if direction == 'hor':
                    x += 20
                else:
                    y += 20
            self.walls.append(tmp_wall)

    def printRandomWalls(self):
        for wall in self.walls:
            for cords in wall:
                pygame.draw.rect(self.scr, (0, 0, 0), [cords['x'] // 20 * 20, cords['y'] // 20 * 20, 20, 20])

    def getBestScore(self):
        inputFile = open("score.txt")
        lines = inputFile.readlines()

        score = 0
        if len(lines) > 0:
            score = int(lines[0])
					 
        inputFile.close() 

        return score

    def saveScore(self):
        for snake in self.snakes:
            inputFile = open("score.txt", "r+")
            
            if snake.score > self.getBestScore():
                inputFile.write(str(snake.score))

            inputFile.close() 		  	   		 						 

    def displayScore(self):
        for k, snake in enumerate(self.snakes):
            display = pygame.font.SysFont(None, 25)
            
            if k == 0:
                value = display.render("Score: " + str(snake.score), True, snake.color)
                self.scr.blit(value, [0, 0])
            else:
                value = display.render("Score: " + str(snake.score), True, snake.color)
                self.scr.blit(value, [0, 30])


    def displayBestScore(self):
        display = pygame.font.SysFont(None, 25)
        value = display.render("Best score: " + str(self.best_score), True, (0, 0, 0))
        self.scr.blit(value, [0, 60])

    def verifyLoose(self, snake, snake_pos):
        for coo in snake.coos:
            if coo[0] >= 1000 or coo[0] < 0 or coo[1] >= 800 or coo[1] < 0:
                self.game_over = True

        for coo in snake.coos[:-1]:
            if coo == snake_pos and snake.ghost is False:
                self.game_over = True

        for coo in self.snakes[0].coos:
            if coo in self.snakes[1].coos and (self.snakes[0].ghost is False and self.snakes[1].ghost is False):
                self.game_over = True

        for coo in snake.coos:
            for wall in self.walls:
                for square in wall:
                    if coo[0] == square['x'] and coo[1] == square['y'] and snake.ghost is False:
                        self.game_over = True


    def printSnake(self):
        for snake in self.snakes:
            snake_pos = [snake.x, snake.y]
            snake.coos.append(snake_pos)
            
            if len(snake.coos) > snake.height:
                del snake.coos[0]

            self.verifyLoose(snake, snake_pos)

            for coo in snake.coos:
                pygame.draw.rect(self.scr, snake.color, [coo[0] // 20 * 20, coo[1] // 20 * 20, snake.size, snake.size])

    def start(self):
        clock = pygame.time.Clock()
        food_types = []
        food_types.append({'label': 'normal', 'color': (255,0,0)})
        food_types.append({'label': 'bad_apple', 'color': (150, 0, 0)}) if self.settings['bad_apple'] else ""
        food_types.append({'label': 'super_apple', 'color': (255,215,0)}) if self.settings['super_apple'] else ""
        food_types.append({'label': 'ghost_apple', 'color': (255,255,255)}) if self.settings['ghost_apple'] else ""

        if self.settings['random_walls']:
            self.setRandomWalls()

        while not self.game_over:
            self.scr.fill(self.color)
            self.printRandomWalls()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.game_over = True
                
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        self.snakes[0].go_to = "LEFT"
                    elif event.key == pygame.K_RIGHT:
                        self.snakes[0].go_to = "RIGHT"
                    elif event.key == pygame.K_UP:
                        self.snakes[0].go_to = "UP"
                    elif event.key == pygame.K_DOWN:
                        self.snakes[0].go_to = "DOWN"

                    if event.key == pygame.K_q:
                        self.snakes[1].go_to = "LEFT"
                    elif event.key == pygame.K_d:
                        self.snakes[1].go_to = "RIGHT"
                    elif event.key == pygame.K_z:
                        self.snakes[1].go_to = "UP"
                    elif event.key == pygame.K_s:
                        self.snakes[1].go_to = "DOWN"

            for snake in self.snakes:
                self.food.eatFood(snake, food_types)
                snake.changeDirection()

            self.printSnake()

            self.displayScore()
            self.displayBestScore()

            pygame.display.update()

            clock.tick(10)
        
        self.end()