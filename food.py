import random
import pygame
    
class Food:
    def __init__(self, scr):
        self.scr = scr
        self.color = (255,0,0)
        self.size = 20
        self.x = random.randint(0, 1000) // 20 * 20
        self.y = random.randint(0, 800) // 20 * 20
        self.type = 'normal'

    def updateGameState(self, snake, types_availables):
        self.x = random.randint(0, 1000) // 20 * 20
        self.y = random.randint(0, 800) // 20 * 20

        if snake.ghost is True and self.type is not 'ghost_apple':
            snake.ghost = False

        if self.type == 'bad_apple':
            if snake.height > 1:
                snake.height -= 1
                snake.score -= 1
                snake.coos = snake.coos[:-1]
        elif self.type == 'super_apple':
            snake.height += 2
            snake.score += 2
        elif self.type == 'ghost_apple':
            snake.ghost = True
        else:
            snake.height += 1
            snake.score += 1


        type = random.choice(types_availables)
        self.type = type['label']
        self.color = type['color']

    def eatFood(self, snake, types_availables):
        if self.x == snake.x and self.y == snake.y:
            self.updateGameState(snake, types_availables)

        pygame.draw.rect(self.scr, self.color, [self.x // 20 * 20, self.y // 20 * 20, self.size, self.size])